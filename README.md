# org-php-hackme

Sample SecureFlag exercise image.

A SecureFlag's exercise image is a Docker image that contains an intentionally vulnerable application and all the development tools required to run, analyse, and remediate the security issue as an exercise on the SecureFlag platform.

This repository is the result of the [SDK tutorial](https://openplatform.secureflag.com/#/sdk/create-image) on how to create an exercise image.

To build and run this image, follow this steps.

1. Install the [SecureFlag SDK](https://openplatform.secureflag.com/#/sdk/setup-sfsdk)
2. Clone this repository

    ```shell
    git clone https://gitlab.com/secureflag-community/org-php-hackme
    ```
    
3. Add a new image using the cloned repository as build folder

   ```shell
   sfsdk img-add org-php-hackme --build-dir org-php-hackme/
   ```

4. Build the image

   ```shell
   sfsdk img-build org-php-hackme
   ```
   
5. Run the image

   ```shell
   sfsdk img-run org-php-hackme --force
   ```

6. Connect to the running image using an RDP client of your choice or use `sfsdk img-shell org-php-hackme` to obtain a root shell.

Read the SecureFlag Open Platform [documentation](https://openplatform.secureflag.com/) to learn more.