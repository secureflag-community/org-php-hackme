FROM secureflag/sf-community

RUN apt-get update \
    && apt-get -y install php-cli

COPY fs/ /
RUN chown -R sf:sf /home/sf
